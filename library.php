<?php
include_once("includes/PHP-functions/utilityFunctions.php");

$pageTitle = "Library page";

include(ROOT . "includes/openDBconn.php");
// MARK: Query 1: Select from BOOK
$query = "select book_id, title, author, release_year, cover_style from BOOK";
$queryOneResults = mysqli_query($db, $query);

$queryOutput = [];
while ($bookRow = mysqli_fetch_array($queryOneResults)) {
    $queryOutput = array_merge($queryOutput, [$bookRow]);
}
$books = $queryOutput;
//
include(ROOT . "includes/closeDBconn.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(ROOT . "includes/UI/headHTML.php"); ?>

    <!-- JQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
</head>

<body>
    <?php
    include(ROOT . "includes/UI/mainUI.php");

    echo $displayPageTitleOpt(null);
    ?>

    <?php
    include_once(ROOT . "includes/PHP-functions/bookCover.php");

    // MARK: Display book tiles
    // MAP book to HTML tile -> REDUCE tiles into string
    $bookTitles = array_reduce(array_map('drawBookTileWithActions', $books), reducedWith(''));

    echo $bookTitles;
    ?>

    <?php
    include(ROOT . "includes/UI/mainUI-close.php");
    ?>
</body>

<script>
    <?php include(ROOT . "includes/JS/toast.php"); ?>

    // Redirects with POST. Reference: https://stackoverflow.com/a/28532801/5856760
    function redirectPost(location, args) {
        var form = '';
        $.each(args, function(key, value) {
            form += '<input type="hidden" name="' + value.name + '" value="' + value.value + '">';
            form += '<input type="hidden" name="' + key + '" value="' + value.value + '">';
        });
        $('<form action="' + location + '" method="POST">' + form + '</form>').submit();
    }

    // MARK: Delete
    // Deletes translation if all values passed.
    // If lang_code is null, deletes all translations and corresponding edition.
    // If lang_code and edition_id are null, delete all translations/editions and book.
    function submitDelete(book_id, edition_id, lang_code) {
        var datastring = [JSON.parse('{ "name":"book_id", "value":"' + book_id + '" }')];
        if (edition_id != null) {
            datastring = datastring.concat(JSON.parse('{ "name":"edition_id", "value":"' + edition_id + '" }'));
        }
        if (lang_code != null) {
            datastring = datastring.concat(JSON.parse('{ "name":"lang_code", "value":"' + lang_code + '" }'));
        }

        if (!confirm("Confirm delete")) {
            return false;
        }

        redirectPost("processing/doDelete.php", datastring);
    }
</script>

</html>