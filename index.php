<?php

// PROCESSES LOGIN REDIRECT

include_once("includes/PHP-functions/utilityFunctions.php");

// MARK: Validate request
$elementsKeys = ["userID" => 1, "password" => 1];
try {
    // Array of all keys and retrieved values
    $posted = validateRetrievedValues($_POST, $elementsKeys, 'processInput');
} catch (Exception $e) {
    header("Location:/login.php");
    exit;
}

// MARK: Hash password
$iv = get_salt($posted["userID"]) ?? get_salt();
$psw_enc = hash_standard($posted["password"], $iv);

include(ROOT . "includes/openDBconn.php");
// MARK: Query 1: Select from USER
$query = 'select userID from USER where userID="' . $posted["userID"] . '" and password="' . $psw_enc . '"';
$queryResults = mysqli_query($db, $query);
//
include(ROOT . "includes/closeDBconn.php");

// MARK: Redirect
if ($queryResults && $queryResults->num_rows > 0) {
    header("Location:library.php");
} else {
    returnError("Try different login info", "login.php");
}
