<?php
include_once("includes/PHP-functions/utilityFunctions.php");
include_once(ROOT . "includes/PHP-functions/langOption.php");

// MARK: Validate request
$elementsKeys = ["book" => 1, "edition" => 1, "lang" => 1];
try {
    $posted = validateRetrievedValues($_GET, $elementsKeys, null);
} catch (Exception $e) {
    returnError("No book found", "library.php");
}

// MARK: Declare IDs
$book_id = $posted["book"];
$edition_id = $posted["edition"];
$lang_code = $posted["lang"];

include(ROOT . "includes/openDBconn.php");
// MARK: Query: Select from BOOK
$query = "select book_id, title, author, description, release_year, cover_style from BOOK where book_id=" . $book_id;
$queryResults = mysqli_query($db, $query);
$row = mysqli_fetch_array($queryResults) ?? returnError("Error retrieving book " . $book_id, "library.php");
$book = $row;
//
// MARK: Query: Select from BOOK_EDITION
$query = "select * from BOOK_EDITION where book_id=" . $book_id;
$queryResults = mysqli_query($db, $query);

$queryOutput = [];
while ($editionRow = mysqli_fetch_array($queryResults)) {
    $queryOutput = array_merge($queryOutput, [$editionRow]);
}
$bookEditions = $queryOutput;
//
// MARK: Query: Select from BOOK_TRANSLATION
$query = "select * from BOOK_TRANSLATION where book_id=" . $book_id;
$queryResults = mysqli_query($db, $query);

$queryOutput = [];
while ($translationRow = mysqli_fetch_array($queryResults)) {
    $queryOutput = array_merge($queryOutput, [$translationRow]);
}
$bookTranslations = $queryOutput;
//
include(ROOT . "includes/closeDBconn.php");


// MARK: Declare variables
// From editions
$ed_name = selectColumn("ed_name", $bookEditions, ["edition_id" => $edition_id])[0];
$author = $book["author"];
$ed_description = selectColumn("description", $bookEditions, ["edition_id" => $edition_id])[0];
// From translations
$trans_title = selectColumn("title", $bookTranslations, ["edition_id" => $edition_id, "lang_code" => $lang_code])[0] ?? $book["title"];
$translator = selectColumn("translator", $bookTranslations, ["edition_id" => $edition_id, "lang_code" => $lang_code])[0];
$epub_path = selectColumn("epub_path", $bookTranslations, ["edition_id" => $edition_id, "lang_code" => $lang_code])[0];
$web_path = selectColumn("web_path", $bookTranslations, ["edition_id" => $edition_id, "lang_code" => $lang_code])[0];


// Set the title
$pageTitle = ($trans_title) . " (" . $book["release_year"] . ")";

/** Prints editions for a book as a list. */
function displayEditions(array $bookEditions, int $edition_id, string $lang_code, array $bookTranslations): string
{
    foreach ($bookEditions as $bookEdition) {
        $isCurrent = $bookEdition["edition_id"] == $edition_id;
        // a tag
        $a_href = 'href="view.php?book=' . $bookEdition["book_id"] . '&edition=' . $bookEdition["edition_id"] . '&lang=' . ($bookEdition["edition_id"] == $edition_id ? $lang_code : "eng") . '"';
        $a_class = 'class="mdl-navigation__link mdl-js-button' . ($isCurrent ? '"' : ' mdl-button--accent"');
        $a = '<a ' . ($isCurrent ? '' : $a_href) . ' ' . $a_class . '>' . $bookEdition["ed_name"] . '</a>';

        // Get a list of translations for edition. FILTER editions -> SORT by lang_code
        $editionTranslations = sorting(selectRow($bookTranslations, ["edition_id" => $bookEdition["edition_id"]]), comparedBy('lang_code'));

        $selectOptions = drawTranslations($editionTranslations, $lang_code, $isCurrent);

        $output = ($output ?? '') . '
        <div class="mdl-list__item mdl-list__item--three-line mdl-menu__item--full-bleed-divider">
            <span class="mdl-list__item-primary-content">
                <span>' . $a . '</span>
                <span class="mdl-list__item-text-body">' . $bookEdition["release_year"] . ' </span>
            </span>
            <span class="mdl-list__item-secondary-content">
                <span class="mdl-list__item-secondary-info">Language</span>
                    <select class="language" onchange="return changeLangPage(this);">' .
            array_reduce($selectOptions, reducedWith(''))  . ' </select>
            </span>
        </div>';
    }

    return "<nav class=\"mdl-list\">" . $output . '</nav>';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(ROOT . "includes/UI/headHTML.php"); ?>

    <!-- JQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
</head>

<body>
    <?php
    include(ROOT . "includes/UI/mainUI.php");
    ?>

    <div class="mdl-layout-spacer"></div>
    <div class="mdl-cell mdl-cell--12-col mdl-cell--10-col-tablet">
        <div class="mdl-grid">
            <!-- MARK: Book cover and actions -->
            <div class="mdl-cell mdl-cell--8-col-tablet">
                <div>
                    <?php
                    // Display book cover tile
                    include_once(ROOT . "includes/PHP-functions/bookCover.php");

                    echo drawBookTileNoActions($book);
                    ?>

                    <?php
                    // Read button if a book file exists on the server
                    if ($epub_path) {
                        echo ' 
                    <button onclick="window.location.href = \'reader.php?file=' . $epub_path . '\';" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent round-button full-width"> Read </button>
                    <div style="height: 24px"></div>';
                    };
                    // Get a copy button if a web path is known
                    if ($web_path) {
                        echo '
                    <button onclick="window.location.href = \'' . $web_path . '\';" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect round-button full-width"> Get a copy </button>';
                    };
                    ?>
                </div>
            </div>
            <div class="mdl-layout-spacer"></div>

            <!-- MARK: Edition info -->
            <div class="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet">
                <!-- MARK: Title and author -->
                <div class="full-width">
                    <?php echo $displayPageTitleOpt($ed_name .  ": " . $lang_code); ?>
                    <p class="mdl-cell full-width">
                        <?php echo $author . (($book["author"] == $translator) ? "" : ", translated by " . $translator); ?>
                    </p>
                </div>

                <!-- About this edition and actions -->
                <div class="full-width">
                    <div class="v-align">
                        <h5> About this edition </h5>
                        <div class="mdl-layout-spacer"></div>
                        <div>
                            <a href="update.php?book=<?php echo $book_id . '&edition=' . $edition_id ?>">
                                <button class="mdl-button mdl-js-button mdl-button--fab drawer-solid main-bg secondary">
                                    <i class="material-icons">edit</i>
                                </button>
                            </a>
                            <button class="mdl-button mdl-js-button mdl-button--fab drawer-solid main-bg secondary" onclick="return submitDelete(<?php echo $book_id . ", " . $edition_id . ", null" ?>)">
                                <i class="material-icons">delete_forever</i>
                            </button>
                        </div>
                    </div>
                    <!-- Description -->
                    <p>
                        <?php
                        echo $ed_description ?? "No information provided.";
                        ?>
                    </p>

                    <!-- MARK: Book info -->
                    <div class="v-align">
                        <h5> About book </h5>
                        <div class="mdl-layout-spacer"></div>
                        <div>
                            <a href="update.php?book=<?php echo $book_id ?>">
                                <button class="mdl-button mdl-js-button mdl-button--fab drawer-solid main-bg secondary">
                                    <i class="material-icons">edit</i>
                                </button>
                            </a>
                            <button class="mdl-button mdl-js-button mdl-button--fab drawer-solid main-bg secondary" onclick="return submitDelete(<?php echo $book_id . ', null, null' ?>)">
                                <i class="material-icons">delete_forever</i>
                            </button>
                        </div>
                    </div>
                    <div>
                        <p><?php echo $book["description"]; ?></p>
                    </div>

                    <!-- MARK: Language/translation -->
                    <div class="v-align">
                        <h5> Language/translation </h5>
                        <div class="mdl-layout-spacer"></div>
                        <div>
                            <a href="update.php?book=<?php echo $book_id . '&edition=' . $edition_id . '&lang=' . $lang_code ?>">
                                <button class="mdl-button mdl-js-button mdl-button--fab drawer-solid main-bg secondary">
                                    <i class="material-icons">edit</i>
                                </button>
                            </a>
                            <button class="mdl-button mdl-js-button mdl-button--fab drawer-solid main-bg secondary" onclick="return submitDelete(<?php echo $book_id . ", " . $edition_id . ", '" . $lang_code  . "'" ?>)">
                                <i class="material-icons">delete_forever</i>
                            </button>
                        </div>
                    </div>
                    <table class=" layout-stroke full-width" style="table-layout:fixed;">
                        <thead>
                            <th class="mdl-data-table__cell--non-numeric">LANGUAGE</th>
                            <th class="mdl-data-table__cell--non-numeric">FILE</th>
                            <th class="mdl-data-table__cell--non-numeric">LINK</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                echo '<td class="mdl-data-table__cell--non-numeric">' . $lang_code . '</td>';
                                echo '<td class="mdl-data-table__cell--non-numeric">' . (isset($epub_path) ? 'Available' : 'Not found') . '</td>';
                                echo '<td class="mdl-data-table__cell--non-numeric">' . (isset($web_path) ? 'Available' : 'Not found') . '</td>';
                                ?>
                            </tr>
                        </tbody>
                    </table>

                    <!-- MARK: All editions -->
                    <div class="full-width">
                            <h5> Editions <?php echo '(' . count($bookEditions) . ')' ?> </h5>
                            <div class="layout-stroke">
                                <?php echo displayEditions($bookEditions, $edition_id, $lang_code, $bookTranslations); ?></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mdl-layout-spacer"></div>

    <?php
    include(ROOT . "includes/UI/mainUI-close.php");
    ?>
</body>

<script>
    <?php include(ROOT . "includes/JS/toast.php"); ?>

    // Redirects with POST. Reference: https://stackoverflow.com/a/28532801/5856760
    function redirectPost(location, args) {
        var form = '';
        $.each(args, function(key, value) {
            form += '<input type="hidden" name="' + value.name + '" value="' + value.value + '">';
            form += '<input type="hidden" name="' + key + '" value="' + value.value + '">';
        });
        $('<form action="' + location + '" method="POST">' + form + '</form>').submit();
    }

    // MARK: Delete
    // Deletes translation if all values passed.
    // If lang_code is null, deletes all translations and corresponding edition.
    // If lang_code and edition_id are null, delete all translations/editions and book.
    function submitDelete(book_id, edition_id, lang_code) {
        var datastring = [JSON.parse('{ "name":"book_id", "value":"' + book_id + '" }')];
        if (edition_id != null) {
            datastring = datastring.concat(JSON.parse('{ "name":"edition_id", "value":"' + edition_id + '" }'));
        }
        if (lang_code != null) {
            datastring = datastring.concat(JSON.parse('{ "name":"lang_code", "value":"' + lang_code + '" }'));
        }

        if (!confirm("Confirm delete")) {
            return false;
        }

        redirectPost("processing/doDelete.php", datastring);
    }

    // MARK: Change language
    // Execute on selection. Reference: https://stackoverflow.com/a/7858323
    function changeLangPage(selectBox) {
        // Change page with GET using JS. Reference: https://stackoverflow.com/a/3846944
        const urlParams = new URLSearchParams(window.location.search);

        window.location = "view.php?book=" + urlParams.get("book") + "&edition=" + selectBox.value.split("-")[0] + "&lang=" + selectBox.value.split("-")[1];
    }
</script>

</html>