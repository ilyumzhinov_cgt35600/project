<?php
include_once("includes/PHP-functions/utilityFunctions.php");
include(ROOT . "includes/PHP-functions/loginForm.php");
include_once(ROOT . "includes/PHP-functions/pageTitle.php");

$pageTitle = "Login page";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(ROOT . "includes/UI/headHTML.php"); ?>
</head>

<body>
    <div class="mdl-layout mdl-js-layout">
        <main class="mdl-layout__content">
            <div class="mdl-grid">
                <div class="mdl-layout-spacer"></div>
                <div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet">
                    <div class="full-width">
                        <?php
                        // Display page title
                        echo drawnTitle($pageTitle)("Welcome to the Book Library's website!");
                        ?>

                        <div style="height: 24px;"> </div>

                        <div class="layout-stroke" style="height:fit-content;">
                            <?php
                            // MARK: Display login form
                            $btnLog_in = displaySubmitBtn("Enter", null);

                            echo displayLoginForm(null, 0, true, [$btnLog_in], ["action" => ["location" => "index.php", "method" => "POST"]]);
                            ?>
                        </div>

                        <!-- MARK: Create login option -->
                        <div style="height: 24px;"> </div>
                        <div class="secondary full-width" style="text-align:center"> or </div>
                        <a href="loginCreate.php">
                            <button type="button" class="mdl-button mdl-js-button round-button full-width"> Create login</button>
                        </a>
                    </div>
                </div>
                <div class="mdl-layout-spacer"></div>
                <?php
                include(ROOT . "includes/UI/mainUI-close.php");
                ?>
</body>

<script>
    <?php include(ROOT . "includes/JS/toast.php"); ?>
</script>

</html>