<?php

// PAGE FOR USER ACCOUNT CREATION

include_once("includes/PHP-functions/utilityFunctions.php");
include(ROOT . "includes/PHP-functions/createLoginForm.php");
include(ROOT . "includes/PHP-functions/pageTitle.php");

$pageTitle = "Create login";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(ROOT . "includes/UI/headHTML.php"); ?>
</head>

<body>
    <div class="mdl-layout mdl-js-layout">
        <main class="mdl-layout__content">

            <div class="mdl-grid">
                <div class="mdl-layout-spacer"></div>
                <div class="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet">
                    <div class="full-width">
                        <?php
                        // Display page title
                        echo drawnTitle($pageTitle)(null);
                        ?>

                        <div style="height: 24px;"> </div>

                        <div class="layout-stroke" style="height:fit-content;">
                            <?php
                            // MARK: Display login form
                            $btnLog_in = displaySubmitBtn("Create", null);

                            $submitAction = ["action" => ["location" => 'processing/doCreateLogin.php', "method" => "POST"]];

                            echo displayCreateLoginForm(null, 0, true, [$btnLog_in], $submitAction);
                            ?>
                        </div>
                    </div>
                </div>
                <div class="mdl-layout-spacer"></div>
                <?php
                include(ROOT . "includes/UI/mainUI-close.php");
                ?>
</body>

<script>
    <?php include(ROOT . "includes/JS/toast.php"); ?>
</script>

</html>