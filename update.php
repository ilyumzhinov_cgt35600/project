<?php
include_once("includes/PHP-functions/utilityFunctions.php");
// Import code to display GUI
include(ROOT . "includes/PHP-functions/bookForm.php");
include(ROOT . "includes/PHP-functions/editionForm.php");
include(ROOT . "includes/PHP-functions/translationForm.php");

// Set values
$book_id = $_GET["book"];

if (!isset($_GET["book"])) {
    returnError("Such book does not exist!", "library.php");
} else {
    // Set mode 
    $pageMode = isset($_GET["lang"]) ? 2 : (isset($_GET["edition"]) ? 1 : 0);
}

include(ROOT . "includes/openDBconn.php");
// MARK: Query: Select from BOOK
$query = "select book_id, title, author, description, release_year, cover_style from BOOK where book_id=" . $book_id;
$queryResults = mysqli_query($db, $query);
$row = mysqli_fetch_array($queryResults) ?? returnError("Error retrieving book " . $book_id, "library.php");
$book = $row;
//
// MARK: Query: Select from BOOK_EDITION
$query = "select * from BOOK_EDITION where book_id=" . $book_id;
$queryResults = mysqli_query($db, $query);
$queryOutput = [];
while ($editionRow = mysqli_fetch_array($queryResults)) {
    $queryOutput = array_merge($queryOutput, [$editionRow]);
}
$bookEditions = $queryOutput;
//
// MARK: Query: Select from BOOK_TRANSLATION
$query = "select * from BOOK_TRANSLATION where book_id=" . $book_id;
$queryResults = mysqli_query($db, $query);

$queryOutput = [];
while ($translationRow = mysqli_fetch_array($queryResults)) {
    $queryOutput = array_merge($queryOutput, [$translationRow]);
}
$bookTranslations = $queryOutput;
//
include(ROOT . "includes/closeDBconn.php");

$edition_id = $_GET["edition"] ?? 1;
// Get language or default
$lang_code = $_GET["lang"] ?? selectColumn("lang_code", $bookTranslations, ["edition_id" => $edition_id])[0];

// MARK: Selected ROWS
$bookEditionTranslations = selectRow($bookTranslations, ["edition_id" => $edition_id]);

// Set the title
$pageTitle = "Update page";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(ROOT . "includes/UI/headHTML.php"); ?>

    <!-- JQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <!-- Collapsible elements -->
    <style>
        .content {
            display: none;
        }
    </style>
</head>

<body>
    <?php include(ROOT . "includes/UI/mainUI.php");

    echo $displayPageTitleOpt(null);
    ?>

    <?php
    // MARK: Action buttons
    // Reference: https://stackoverflow.com/a/942826
    function btnView($id)
    {
        return displayActionButton("View", 'showTab(this, ' . $id . ')', 'lightgrey');
    };

    function btnActions(int $formID, int $pageMode): array
    {
        switch ($pageMode) {
            case 0:
                $mode = 'book';
                break;
            case 1:
                $mode = 'edition';
                break;
            case 2:
                $mode = 'lang';
                break;
            default:
                $mode = '';
        }

        $btnSubmit = displayActionButton("Update", "submitForm(" . $formID . ", '" . $mode . "')", null);

        return $formID == $pageMode ? [$btnSubmit] : [btnView($formID)];
    };
    ?>
    <!-- MARK: Display forms -->
    <div class="layout-stroke mdl-cell mdl-cell--12-col">
        <?php
        echo displayBookForm($book, 0, $pageMode == 0 ? true : false, btnActions(0, $pageMode), null);
        ?>
    </div>
    <div class="layout-stroke mdl-cell mdl-cell--12-col">
        <?php
        echo displayEditionForm($bookEditions, $edition_id, 1, $pageMode == 1 ? true : false, btnActions(1, $pageMode), null)
        ?>
    </div>
    <div class="layout-stroke mdl-cell mdl-cell--12-col">
        <?php
        echo displayTranslationForm($bookEditionTranslations, $lang_code, 2, $pageMode == 2 ? true : false, btnActions(2, $pageMode), null, null)
        ?>
    </div>

    <?php include(ROOT . "includes/UI/mainUI-close.php"); ?>
</body>

<script>
    <?php include(ROOT . "includes/JS/toast.php"); ?>

    // MARK: Multiple step tabs
    // Reference: https://www.w3schools.com/howto/howto_js_form_steps.asp
    showTab(null, <?php echo $pageMode; ?>);

    // Displays a tab based on the content id.
    // Reference: https://www.w3schools.com/howto/howto_js_collapsible.asp
    function showTab(sender, n) {
        var content = document.getElementsByClassName("content")[n];

        if ((content.style.display || "none") == "none") {
            content.style.display = "inline";
            if (sender != null) {
                sender.innerHTML = 'Close'
            }
        } else {
            content.style.display = "none"
            if (sender != null) {
                sender.innerHTML = 'View'
            }
        }
    }

    // MARK: Submit edited form
    // Submits edited form
    function submitForm(formID, mode) {
        var datastring = $("#editForm-" + formID).serializeArray();
        datastring = datastring.concat(JSON.parse('{ "name":"mode", "value":"' + mode + '" }'));

        // Pass ID from HTTP tags
        const urlParams = new URLSearchParams(window.location.search);
        datastring = datastring.concat(JSON.parse('{ "name":"book_id", "value":"' + urlParams.get("book") + '" }'));
        if (urlParams.get("edition") != null) {
            datastring = datastring.concat(JSON.parse('{ "name":"edition_id", "value":"' + urlParams.get("edition") + '"}'));
        }
        if (urlParams.get("lang") != null) {
            datastring = datastring.concat(JSON.parse('{ "name":"lang_code", "value":"' + urlParams.get("lang") + '"}'));
        }

        // Redirect with POST. Reference: https://stackoverflow.com/a/28532801/5856760
        function redirectPost(location, args) {
            var form = '';
            $.each(args, function(key, value) {
                form += '<input type="hidden" name="' + value.name + '" value="' + value.value + '">';
                form += '<input type="hidden" name="' + key + '" value="' + value.value + '">';
            });
            $('<form action="' + location + '" method="POST">' + form + '</form>').submit();
        }

        redirectPost('processing/doUpdate.php?mode=' + mode, datastring);
    }
</script>

</html>