<?php

// CONTAINS EPUB READER RENDERER USING JS AND DOWNLOADS THE EPUB FILE FROM THE SERVER USING AN API

include_once("includes/PHP-functions/utilityFunctions.php");

$bookPath = isset($_GET["file"]) ? $_GET["file"] : null;

if (!isset($bookPath)) {
    setcookie("errorMessage", "No book found", time() + 2);
    header("Location:library.php");
    exit;
}

// Set the title
$pageTitle = "Online Reader";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(ROOT . "includes/UI/headHTML.php"); ?>

    <!-- Epub renderer JS library. Reference: https://github.com/futurepress/epub.js -->
    <script src="https://cdn.jsdelivr.net/npm/epubjs/dist/epub.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
</head>

<body>
    <div id="reader"></div>

    <!-- MARK: Close reader -->
    <button class="mdl-button mdl-js-button drawer-solid secondary v-align" style="position: absolute;top: 0%;background:none" onclick="window.location.href = 'library.php'">
        <i class="material-icons">close</i>
        Close
    </button>

    <!-- MARK: Turn pages -->
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab drawer-solid secondary" style="position: absolute;top: 50%;margin-top: -32px;right:0px;background:none" onclick="turnNextPage();" ?>
        <i class="material-icons">keyboard_arrow_right</i>
    </button>
    </div>
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--fab drawer-solid secondary" style="position: absolute;top: 50%;margin-top: -32px;background:none" onclick="turnPrevPage();" ?>
        <i class="material-icons">keyboard_arrow_left</i>
    </button>
</body>

<script>
    var path = '<?php echo "API/downloadBook.php?file=" . $bookPath ?>';
    var book = ePub(path);
    var rendition = book.renderTo("reader", {
        method: "default"
    });
    rendition.display();

    // Turn pages. Reference: https://github.com/futurepress/epub.js/issues/829
    function turnNextPage() {
        rendition.next();
    }

    function turnPrevPage() {
        rendition.prev();
    }
</script>

</html>