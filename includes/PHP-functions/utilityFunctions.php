<?php

// CONTAINS FUNCTIONS FOR GENERAL PHP PROGRAMMING:
// Passing errors and input, array processing

include_once(__DIR__ . "/../../config/config.php");

// MARK: Errors
/** Saves 2 lines of code to print an error message */
function returnError(string $message, string $headerLocation)
{
    setcookie("errorMessage", $message, time() + 2);
    header("Location:" . '/' . $headerLocation);
    exit;
}


// MARK: Input processing
/** Processes input to save string.
 * str -> trim spaces -> add slashes
 */
function processInput(?string $str): string
{
    return addslashes(trim($str));
}


// MARK: Reduce functions
/** A shortcut to combine elements by summation and to insert a symbol between elements */
function reducedWith($symbol)
{
    return function ($accum, $new) use ($symbol) {
        return $accum . $symbol . $new;
    };
}


// MARK: Sort functions
/** Makes a comparison between 2 column values in array.
 * @param string $columnName Column from which values to compare.
 * @return Returns a closure to compare 2 arrays.
 */
function comparedBy(string $columnName)
{
    return function (array $a, array $b) use ($columnName) {
        return strcmp($a[$columnName], $b[$columnName]);
    };
}

/** Sorts values using a compare function and returns the sorted array.
 * @return Returns a sorted array.
 */
function sorting(array $arr, $cmpFunction): array
{
    $temp = $arr;
    usort($temp, $cmpFunction);

    return $temp;
}


// MARK: Table selections
/** Selects a column from table using SELECT column FROM table WHERE [col1=>value, ...].
 * @param string $select Column to select.
 * @param array $from Table (array of arrays) to process.
 * @param ?array $where List of conditions as column=match pairs. Declare as array: [col1=>value,col=>value].
 * @return ?array Returns a value representing a column array: 0|1|many columns.
 */
function selectColumn(string $select, array $from, ?array $where): ?array
{
    $column = $select;
    $table = $from;
    $conditions = $where;

    foreach ($table as $row) {
        if (isset($conditions)) {
            foreach ($conditions as $col => $value) {
                if ($row[$col] != $value) {
                    $running = null;
                    break;
                }

                $running = $row[$column];
            }
        } else {
            $running = $row[$column];
        }

        if (isset($running)) {
            $result = array_merge($result ?? [], [$running]);
        }
    }

    return isset($result) ? $result : null;
}

/** Selects rows from table using the SELECT * FROM table WHERE [col1=>value, ...] style of arguments.
 * @param array $from Table (array of arrays) to process.
 * @param array $where List of conditions as column=match pairs. Declare as array: [col1=>value,col=>value].
 * @return ?array Returns an array representing table rows with all columns: 0|1|many rows.
 */
function selectRow(array $from, array $where): ?array
{
    $table = $from;
    $conditions = $where;

    foreach ($table as $row) {
        $running = null;

        foreach ($conditions as $col => $value) {
            if ($row[$col] != $value) {
                $running = null;
                break;
            }

            $running = $row;
        }

        if (isset($running)) {
            $result = array_merge($result ?? [], [$running]);
        }
    }

    return isset($result) ? $result : null;
}


// MARK: Validation
/** Validates that values are received for required keys.
 * @param array $post Any array like. E.g. $_POST or $_GET.
 * @param array $forKeys List of keys requirements as key=match pairs: 1 - required, 0 - optional. Declare as array: [key1=>1,key2=>0]. Only lack of required keys will return in an error.
 * @param function $process Optionally, pass input processing function to safely wrap retrieved values.
 * @return array Returns an array guaranteeing values for required keys and having values for some optional keys.
 */
function validateRetrievedValues(array $post, array $forKeys, $process): array
{
    $elementKeys = $forKeys;
    $result = [];
    // Array of all keys and retrieved values
    foreach ($elementKeys as $key => $value) {
        $received = $post[$key];

        if (isset($received) == false && $value == 1) {
            throw new Exception("No value received for required key: " . $key);
        }

        $result[$key] = isset($process) ? $process($received) : $received;
    }

    return $result;
}


// MARK: Reading config file
/** Reads credentials.json file and returns values for key.
 * !!! Check README file in the /config folder.
 * @param string $id Accepts: "mysql-db"|"aes-enc"|"sha-hash" or throws exception. 
 * - Parses "mysql-db": [...].
 * - Parses "aes-enc": ["private_key" in binary, ...].
 * - Parses "sha-hash": [...].
 * @return array Returns an array of key value pairs for that id: [key1=>value, key2=>value].
 */
function read_conf(string $id): array
{
    // Read credentials.json file
    // Reference: https://stackoverflow.com/questions/26300684/how-to-hide-mysql-details-in-github-repo
    $credentials_json = file_get_contents(__DIR__ . "/../../config/config.json");

    $jsonIterator = new RecursiveIteratorIterator(
        new RecursiveArrayIterator(json_decode($credentials_json, TRUE)),
        RecursiveIteratorIterator::SELF_FIRST
    );

    $result = [];

    // Parse JSON and filter values to given id 
    // Reference: https://stackoverflow.com/questions/4343596/how-can-i-parse-a-json-file-with-php
    foreach ($jsonIterator as $key => $val) {
        if (!is_array($val) || $key != $id) {
            continue;
        }

        switch ($id) {
            case "mysql-db":
                // Read data for database as is
                $result[$key] = $val;
                break;
            case "aes-enc":
                // Read data for aes and transform key to binary
                switch ($key) {
                    case "private_key":
                        $result[$key] = base64_decode($val);
                        break;
                    default:
                        $result[$key] = $val;
                }
                break;
            case "sha-hash":
                $result[$key] = $val;
                break;
            default:
                throw new Exception("Configuration for " . $id . " is not supported!");
        }

        $result[$key] = $val;
    }

    return $result[$id];
}

// MARK: Encryption and hashing
/** Gets salt used for encryption and hashing.
 * @param ?string $userID Provide a user ID whose salt to return. By default, generates random salt.
 * @return ?string Random | user's salt | null. Encoded in base64.
 */
function get_salt(?string $userID = null): ?string
{
    // Generate new salt
    if ($userID == null) {
        // Read config
        $conf_aes = read_conf("aes-enc");
        $cipher = $conf_aes["cipher"];

        $iv = base64_encode(openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher)));

        return $iv;
    }

    // Get user salt
    include_once(ROOT . "includes/openDBconn.php");
    // Query 1: Select from USER
    $querySalt = 'select salt from USER where userID="' . $userID . '"';
    $queryResultsSalt = mysqli_query($db, $querySalt);
    //
    include_once(ROOT . "includes/closeDBconn.php");

    if ($row = mysqli_fetch_array($queryResultsSalt)) {
        return $row["salt"];
    }

    return null;
}


/** Encrypts data in the standard way: using "config.json".
 * @param string $iv Provide a specific IV in base64.
 * @return string Encrypted data. Encoded in base64.
 */
function encrypt_standard(string $data, string $iv): string
{
    // Read config
    $conf_aes = read_conf("aes-enc");
    $cipher = $conf_aes["cipher"];
    $key = $conf_aes["private_key"];

    // Encrypt data
    $data_enc = openssl_encrypt($data, $cipher, $key, 0, base64_decode($iv));

    return $data_enc;
}

/** Decrypts data in the standard way: using "config.json" and IV attached to the end of the datastring.
 * @return string Plaintext data.
 */
function decrypt_standard(string $data_enc, string $iv): string
{
    // Read config
    $conf_aes = read_conf("aes-enc");
    $cipher = $conf_aes["cipher"];
    $key = $conf_aes["private_key"];

    // Decrypt data
    $data = openssl_decrypt($data_enc, $cipher, $key, 0, base64_decode($iv));

    return $data;
}

/** Hashes data in the standard way: using "sha3-512".
 * Appends salt to the end of hash to secure it.
 */
function hash_standard(string $data, string $iv): string
{
    // Read config
    $sha_hash = read_conf("sha-hash");

    $data_IV = $data . $iv;

    return hash($sha_hash["function"], $data_IV);
}
