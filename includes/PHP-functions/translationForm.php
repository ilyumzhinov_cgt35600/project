<?php

// CONTAINS FUNCTIONALITY RELATED TO DRAWING AN HTML FORM FOR TRANSLATION INFORMATION'S INPUT, VIEWING AND UPDATING

include_once("includes/PHP-functions/utilityFunctions.php");
include_once(ROOT . "includes/PHP-functions/langOption.php");
include_once(ROOT . "includes/PHP-functions/formEditCommon.php");

// MARK: Edition information
/** Generates a form for translation information.
 * @param ?array $bookEditionTranslations List of translations for a particular book and particular edition. Otherwise, will produce wrong results. If NULL, does not prefill values.
 * @param ?string $lang_code Selected language.
 * @param int $formID Form ID on the page.
 * @param bool $readOnly Indicates whether input fields can be modified.
 * @param ?array $actionsHTML Form JS actions.
 * @param ?array $formEvents Form events' actions like submission or Javascript function. Currently supported:
 * - ["action" => ["location" => "LINK", "method" => "GET"|"POST"]]
 * - ["onsubmit" => "return JSFUNCTION()"]
 * @return string Returns generated HTML form's code to render. 
 */
function displayTranslationForm(
    ?array $bookEditionTranslations,
    ?string $lang_code,
    int $formID,
    bool $readOnly,
    ?array $actionsHTML,
    ?array $formEvents,
    ?string $onselectSelectJS
): string {
    // MARK: Get languages depending if edition is passed
    if ($bookEditionTranslations == null or $lang_code == null) {
        include(ROOT . "includes/openDBconn.php");
        // MARK: Query: Select from LANGUAGE
        $query = "select lang_code from LANGUAGE";
        $queryResults = mysqli_query($db, $query);
        $queryOutput = [];
        while ($row = mysqli_fetch_array($queryResults)) {
            $queryOutput = array_merge($queryOutput, [$row]);
        }
        $globalLanguages = sorting($queryOutput, comparedBy("lang_code"));
        //
        include(ROOT . "includes/closeDBconn.php");

        // Create renderable string of language options
        // MAP option tags -> REDUCE one string
        $langOptions = array_reduce(
            array_map(
                drawnTranslationOption($globalLanguages[0]["lang_code"]),
                $globalLanguages
            ),
            reducedWith('')
        );
    } else {
        // Create renderable string of language options for passed edition
        // MAP option tags -> REDUCE one string
        $langOptions = array_reduce(
            array_map(
                drawnTranslationOption($lang_code),
                $bookEditionTranslations
            ),
            reducedWith('')
        );

        // FILTER lang_code -> SELECT first
        $selectedTranslation = selectRow($bookEditionTranslations, ["lang_code" => $lang_code])[0];
    }

    $actionOnSelect = '" onchange="' . (isset($onselectSelectJS) ? $onselectSelectJS . '"' : 'return false"');

    $disabled = $readOnly ? '' : ' disabled';

    // MARK: Create fields
    $trans_lang_codeField = '<select name="trans_lang_code"' . $actionOnSelect . '>' . $langOptions . ' </select>
    <h6>Optional information</h6>';

    $translatorField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="trans_translator" id="trans_translator" class="mdl-textfield__input"
    ' . $disabled . '
    value="' . ($selectedTranslation["translator"] ?? "") . '" />
    <label title="Translator" for="trans_translator" class="mdl-textfield__label">Author/translator</label>
    </div>';

    $titleField = ' <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="trans_title" id="trans_title" class="mdl-textfield__input"
    ' . $disabled . '
    value="' . ($selectedTranslation["title"] ?? "") . '" />
    <label title="trans_title" for="Translation title" class="mdl-textfield__label">Translation Title</label>
    </div>';

    $epubField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="trans_epub" id="trans_epub" class="mdl-textfield__input" 
    value="' . ($selectedTranslation["epub_path"] ?? "") . '" />
    <label title="Epub" for="trans_epub" class="mdl-textfield__label">Epub Path</label>
    </div>';

    $webPath = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="trans_web" id="trans_web" class="mdl-textfield__input"
    ' . $disabled . '
    value="' . ($selectedTranslation["web_path"] ?? "") . '" />
    <label title="Web link" for="trans_web" class="mdl-textfield__label">Web Link</label>
    </div>';

    return displayForm("Language/translation", $formID, $formEvents, $actionsHTML, [$trans_lang_codeField, $translatorField, $titleField, $epubField, $webPath]);
}
