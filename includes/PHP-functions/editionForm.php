<?php

// CONTAINS FUNCTIONALITY RELATED TO DRAWING AN HTML FORM FOR EDITION INFORMATION'S INPUT, VIEWING AND UPDATING

// MARK: Includes
include_once("includes/PHP-functions/utilityFunctions.php");
include_once(ROOT . "includes/PHP-functions/formEditCommon.php");

// MARK: Edition information
/** Generates a form for edition information.
 * @param ?array $bookEditions List of editions for a particular book. Otherwise, will produce wrong results. If NULL, does not prefill values.
 * @param ?int $edition_id Selected edition.
 * @param int $formID Form ID on the page.
 * @param bool $readOnly Indicates whether input fields can be modified.
 * @param ?array $formEvents Form events' actions like submission or Javascript function. Currently supported:
 * - ["action" => ["location" => "LINK", "method" => "GET"|"POST"]]
 * - ["onsubmit" => "return JSFUNCTION()"]
 * @return string Returns generated HTML form's code to render. 
 */
function displayEditionForm(
    ?array $bookEditions,
    ?int $edition_id,
    int $formID,
    bool $readOnly,
    ?array $actionsHTML,
    ?array $formEvents
): string {
    // Current edition. FILTER edition_id -> Select first
    $bookEdition = selectRow($bookEditions ?? [], ["edition_id" => $edition_id ?? 1])[0] ?? [];

    $disabled = $readOnly ? '' : ' disabled';

    // MARK: Create fields
    $ed_nameField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="ed_name" id="ed_name" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($bookEdition["ed_name"] ?? "") . '" />
    <label title="Edition name" for="ed_name" class="mdl-textfield__label" >Edition Name</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    $releaseYearField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="ed_release_year" id="ed_release_year" pattern="[0-9]{4}" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($bookEdition["release_year"] ?? "") . '" />
    <label title="Release Year" for="ed_release_year" class="mdl-textfield__label">Release Year</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    $optionalField = '<h6>Optional information</h6>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
        <input type="text" name="ed_description" id="ed_description" class="mdl-textfield__input" 
        ' . $disabled . '
        value="' . ($bookEdition["description"] ?? "") . '" />
        <label title="Description" for="ed_description" class="mdl-textfield__label">Description</label>
    </div>';

    return displayForm("Edition", $formID, $formEvents, $actionsHTML, [$ed_nameField, $releaseYearField, $optionalField]);
}
