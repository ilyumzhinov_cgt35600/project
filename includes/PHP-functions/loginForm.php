<?php

// CONTAINS FUNCTIONALITY RELATED TO DRAWING AN HTML FORM FOR LOGIN INFORMATION'S INPUT, VIEWING AND UPDATING

// MARK: Includes
include_once("includes/PHP-functions/utilityFunctions.php");
include_once(ROOT . "includes/PHP-functions/formEditCommon.php");

// MARK: Login form
/** Generates a form for login information.
 * @param ?array $loginData An array of login data fields.
 * @param int $formID Form ID on the page.
 * @param bool $readOnly Indicates whether input fields can be modified.
 * @param ?array $formEvents Form events' actions like submission or Javascript function. Currently supported: ["action" => ["location" => "LINK", "method" => "GET"|"POST"]] and ["onsubmit" => "return JSFUNCTION()"]
 * @return string Returns generated HTML form's code to render. */
function displayLoginForm(
    ?array $loginData,
    int $formID,
    bool $readOnly,
    ?array $actionsHTML,
    ?array $formEvents
): string {
    $disabled = $readOnly ? '' : ' disabled';

    // MARK: Create userID field
    $loginField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="userID" id="userID" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($loginData["userID"] ?? "") . '" />
    <label title="userID" for="userID" class="mdl-textfield__label">UserID</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    // MARK: Create password field
    $passwordField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="password" name="password" id="password" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($loginData["password"] ?? "") . '" />
    <label title="password" for="password" class="mdl-textfield__label">Password</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    return displayForm("Login", $formID, $formEvents, $actionsHTML, [$loginField, $passwordField]);
}
