<?php
// MARK: Edit form actions
/** Renders a button that executes JS function on click. Does not submit the form. */
function displayActionButton(string $label, string $jsAction, ?string $color): string
{
    $color = isset($color) ? ' style="background:' . $color . '"' : '';

    return '<button type="button" onclick="' . $jsAction . '" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent round-button secondary"' . $color . '>' . $label . '</button>';
}

/** Renders a button that submits the form. */
function displaySubmitBtn(string $label, ?string $color): string
{
    $color = isset($color) ? ' style="background:' . $color . '"' : '';

    return '<button type="submit" name="' . $label . '" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent round-button secondary"' . $color . '>' . $label . '</button>';
}

// MARK: Generate typical form
/** Generates a typical form.
 * @param string $formName Form title to display on top.
 * @param int $formID Form ID on the page.
 * @param ?array $formEvents Form events' actions like submission or Javascript function. Currently supported:
 * - ["action" => ["location" => "LINK", "method" => "GET"|"POST"]]
 * - ["onsubmit" => "return JSFUNCTION()"]
 * @param array $fieldsHTML HTML form fields to be displayed.
 * @return string Returns generated HTML form's code to render. */
function displayForm(string $formName, int $formID, ?array $formEvents, ?array $actionsHTML, array $fieldsHTML): string
{
    // Process form actions
    $formact = '';
    if (isset($formEvents)) {
        foreach ($formEvents as $actionName => $action) {
            switch ($actionName) {
                case "action":
                    $formact = $formact . ' action="' . $action["location"] . '" method="' . $action["method"] . '" ';
                    break;
                case "onsubmit":
                    $formact = $formact . ' onsubmit="' . $action . '"';
                    break;
            }
        }
    }

    $actions = array_reduce($actionsHTML ?? [], reducedWith(' '));
    $fields_red = array_reduce($fieldsHTML, reducedWith('<div style="height: 12px;"></div>'));

    return '
    <form id="editForm-' . $formID . '"' . $formact . '>
        <div style="justify-content: space-between;display: flex; align-items: center;">
            <button class="collapsible drawer-solid main-bg">
                <h5> ' . $formName . ' </h5>
            </button>
            <div id="tabActions-' . $formID . '" class="tabActions">
                ' . $actions . '
            </div>
        </div>
        <div class="content" style="height: fit-content">
            <div>
                ' . $fields_red . '
            </div>
        </div>
    </form>';
}
