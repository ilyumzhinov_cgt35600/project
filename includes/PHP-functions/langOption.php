<?php
// MARK: Translations
/** Draws indexed language options */
function drawTranslations(array $editionTranslations, string $lang_code, bool $isSelected): array
{
    $selectOptions = array_map(function ($translation) use ($lang_code, $isSelected) {
        $isCurrent = ($lang_code == $translation["lang_code"] and $isSelected) ? true : false;

        return '<option value="' . $translation["edition_id"] . '-' . $translation["lang_code"] . '"' . ($isCurrent ? ' selected' : '') . '>' . $translation["lang_code"] . '</option>';
    }, $editionTranslations);

    return $selectOptions;
}

/** Draws a language option without edition ID.  
 * @param string $lang_code_current Identify which lang code should be marked as selected.
 * @return Returns A map ready function that receives a translation and given HTML for language option tag. */
function drawnTranslationOption(string $lang_code_current)
{
    return function ($translation) use ($lang_code_current) {
        $isCurrent = $translation["lang_code"] == $lang_code_current;

        return '<option value="' . $translation["lang_code"] . '"' . ($isCurrent ? ' selected' : '') . '>' . $translation["lang_code"] . '</option>';
    };
}