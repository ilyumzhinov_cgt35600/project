<?php

// CONTAINS FUNCTIONALITY RELATED TO DRAWING HTML CODE FOR PAGE TITLE

// MARK: Page title
function drawnTitle(string $pageTitle)
{
    // Reference: https://stackoverflow.com/a/32137411/5856760
    return function (?string $opts) use ($pageTitle) {
        $subtitle = isset($opts) ? '<h5 class="mdl-button--primary" style="margin:0">' . $opts . '</h5>' : '' ;

        return
            '<header class="mdl-cell full-width main-bg" style="margin-bottom:0">' .
            '<h4 style="margin-bottom:0">' . $pageTitle . '</h4>' .
            $subtitle .
            ' </header>';
    };
}