<?php
// MARK: Book cover
/** Draws HTML menu for a book */
function displayActions(int $book_id): string
{
    return '<button id="menu-lower-right-' . $book_id . '" class="mdl-card__menu mdl-button mdl-js-button mdl-button--icon">
        <i class="material-icons">layers</i> </button>' .
        '<nav class="mdl-menu mdl-menu--bottom-right mdl-js-menu" for="menu-lower-right-' . $book_id . '">
            <a href="update.php?' . 'book=' . $book_id . '" class="mdl-menu__item">Update</a>
            <button class="mdl-button mdl-js-button mdl-menu__item full-width" onclick="return submitDelete(' . $book_id . ", null, null" . ')">
                Delete
            </button>
        </nav>';
}

/** Generates HTML code for a book tile with actions. */
function drawBookTileWithActions(array $book): string
{
    $actions = '<div class="mdl-card__actions mdl-card--border">
        <a href="processing/doView.php?book=' . $book["book_id"] . '" class="mdl-button mdl-button--colored mdl-js-button"> View </a>
    </div>' .  displayActions($book["book_id"]);

    return '<div class="mdl-cell" style="margin-bottom: 36px;">
                <div class="mdl-card mdl-shadow--16dp mdl-button mdl-js-button mdl-button--raised mdl-button--accent" style="width: 200px; height: 330px; margin: auto; background:' . $book["cover_style"] . ';">

                    <div class="mdl-card__title mdl-card--expand">
                        <h4 style="width:100%; word-wrap:break-word; font-size: 2vw;">
                            ' . trim($book["title"]) . '
                        </h4>
                    </div>
                    ' . $book["author"] .  $actions . '
                </div>
            </div>';
}

/** Generates HTML code for a book tile without any interactive elements.
 * Provides default values if values not found or null.
 */
function drawBookTileNoActions(?array $book): string
{
    return '
        <div class="mdl-card mdl-shadow--16dp mdl-button mdl-js-button mdl-button--raised mdl-button--accent" style="width: 200px; height: 330px; margin: auto; margin-bottom: 36px; background:' . $book["cover_style"] . ';">

            <div class="mdl-card__title mdl-card--expand">
                <h4 style="width:100%; word-wrap:break-word; font-size: 2vw;">
                    ' . trim($book["title"] ?? 'Unknown Title') . '
                </h4>
            </div>
            ' . ($book["author"]  ?? 'Unknown Author') . '
        </div>';
}
