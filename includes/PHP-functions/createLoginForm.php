<?php

// CONTAINS FUNCTIONALITY RELATED TO DRAWING AN HTML FORM FOR CREATING LOGIN INFORMATION'S
// AND JAVASCRIPT TO PROCESS USER ID AVAILABILITY CHECKS

// MARK: Includes
include_once("includes/PHP-functions/utilityFunctions.php");
include_once(ROOT . "includes/PHP-functions/formEditCommon.php");

// MARK: Create login form
/** Generates a form for login information.
 * @param ?array $loginData An array of login data fields.
 * @param int $formID Form ID on the page.
 * @param bool $editable Indicates whether input fields can be modified.
 * @param ?array $formEvents Form events' actions like submission or Javascript function. Currently supported: ["action" => ["location" => "LINK", "method" => "GET"|"POST"]] and ["onsubmit" => "return JSFUNCTION()"]
 * @return string Returns generated HTML form's code to render. */
function displayCreateLoginForm(
    ?array $loginData,
    int $formID,
    bool $editable,
    ?array $actionsHTML,
    ?array $formEvents
): string {
    $disabled = $editable ? '' : ' disabled';

    // MARK: Create fields
    // Create userID field
    // Calls JS function on input event
    // Reference: 
    $loginField = '<div id="userIDInput" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="userID" id="userID" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($loginData["userID"] ?? "") . '" oninput="checkUserAvailability(this)"/>
    <label title="userID" for="userID" class="mdl-textfield__label">UserID</label>
    <span id="usernameAvailability" class="mdl-textfield__error">Required</span>
    </div>';

    // Create password field
    $passwordField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="password" name="password" id="password" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($loginData["password"] ?? "") . '" />
    <label title="password" for="password" class="mdl-textfield__label">Password</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    // Create password confirmation field
    $passwordConfField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="password" name="passwordConf" id="passwordConf" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($loginData["passwordConf"] ?? "") . '" />
    <label title="passwordConf" for="passwordConf" class="mdl-textfield__label">Confrim password</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    // Create name field
    $nameField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="name" id="name" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($loginData["name"] ?? "") . '" />
    <label title="name" for="name" class="mdl-textfield__label">Full name</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    // Create email field
    $emailField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="email" name="email" id="email" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($loginData["email"] ?? "") . '" />
    <label title="email" for="email" class="mdl-textfield__label">Email</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    return displayForm("User account", $formID, $formEvents, $actionsHTML, [$loginField, $passwordField, $passwordConfField, $nameField, $emailField]);
}
?>

<script>
    // MARK: Display user availability
    // Makes appropriate changes to HTML for user availability
    // status - User availability status
    function displayUserAvailability(status) {
        var errorField = document.getElementById("usernameAvailability"),
            inputField = document.getElementById("userIDInput");

        switch (status) {
            case "available":
                inputField.classList.add("is-upgraded");
                errorField.style.visibility = "hidden";
                break;
            case "not available":
                inputField.classList.add("is-invalid");
                errorField.style.visibility = "visible";
                errorField.innerHTML = "Choose a different UserID";
                break;
            case "blank":
                errorField.style.visibility = "visible";
                errorField.innerHTML = "Required";
                break;
            default:
                errorField.style.visibility = "visible";
                errorField.innerHTML = "Checking...";
                break;
        }
    }

    // MARK: Request user availability
    // Makes a request to the php page that checks the database for username availability
    function checkUserAvailability(elem) {
        var userID = elem.value;

        if (userID.length == 0) {
            displayUserAvailability("blank");
            return;
        }

        const request = new XMLHttpRequest(),
            method = "GET",
            url = 'API/checkUserID.php?mode=ask&userID=' + escape(userID);

        request.open(method, url, true);

        // Asynchrously display results
        request.onreadystatechange = function() {
            if (request.readyState == XMLHttpRequest.DONE && request.status == 200) {
                var response = request.responseText;

                displayUserAvailability(response);
            } else {}
        }

        request.send(null);
    }
</script>