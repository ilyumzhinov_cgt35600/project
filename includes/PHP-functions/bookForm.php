<?php

// CONTAINS FUNCTIONALITY RELATED TO DRAWING AN HTML FORM FOR BOOK INFORMATION'S INPUT, VIEWING AND UPDATING

// MARK: Includes
include_once("includes/PHP-functions/utilityFunctions.php");
include_once(ROOT . "includes/PHP-functions/formEditCommon.php");

// MARK: Book information
/** Generates a form for book information.
 * @param ?array $book An array of the book row.
 * @param int $formID Form ID on the page.
 * @param bool $readOnly Indicates whether input fields can be modified.
 * @param ?array $formEvents Form events' actions like submission or Javascript function. Currently supported:
 * - ["action" => ["location" => "LINK", "method" => "GET"|"POST"]]
 * - ["onsubmit" => "return JSFUNCTION()"]
 * @return string Returns generated HTML form's code to render. */
function displayBookForm(
    ?array $book,
    int $formID,
    bool $readOnly,
    ?array $actionsHTML,
    ?array $formEvents
): string {
    $disabled = $readOnly ? '' : ' disabled';

    // MARK: Create fields
    $titleField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="book_title" id="book_title" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($book["title"] ?? "") . '" />
    <label title="Book title" for="book_title" class="mdl-textfield__label">Title</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    $authorField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="book_author" id="book_author" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($book["author"] ?? "") . '" />
    <label title="Book author" for="book_author" class="mdl-textfield__label">Author</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    $releaseYearField = '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" name="book_release_year" id="book_release_year" pattern="[0-9]{4}" required class="mdl-textfield__input" 
    ' . $disabled . '
    value="' . ($book["release_year"] ?? "") . '" />
    <label title="Release year" for="book_release_year" class="mdl-textfield__label">Release Year</label>
    <span class="mdl-textfield__error">Required</span>
    </div>';

    $optionalField = '<h6>Optional information</h6>
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label full-width">
        <input type="text" name="book_description" id="book_description" class="mdl-textfield__input" 
        ' . $disabled . '
        value="' . ($book["description"] ?? "") . '" />
        <label title="Description" for="book_description" class="mdl-textfield__label">Description</label>
    </div>';

    return displayForm("Book", $formID, $formEvents, $actionsHTML, [$titleField, $authorField, $releaseYearField, $optionalField]);
}
