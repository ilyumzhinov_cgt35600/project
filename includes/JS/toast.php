// Show error Toast when loading the page
// Reference: https://stackoverflow.com/a/45701842/5856760
r(function() {
var notification = document.querySelector('.mdl-js-snackbar');

var msg = '<?php echo ($_COOKIE["errorMessage"] ?? ""); ?>';

if (msg.length > 0) {
notification.MaterialSnackbar.showSnackbar({
message: msg,
timeout: 4000
});
}
});

function r(f) {
/in/.test(document.readyState) ? setTimeout('r(' + f + ')', 20) : f()
}