<?php

include_once("PHP-functions/utilityFunctions.php");

// MARK: Read credentials.json file
$mysql_auth = read_conf("mysql-db");

// MARK: Make connection
$db = mysqli_connect($mysql_auth["host"], $mysql_auth["user"], $mysql_auth["password"], $mysql_auth["database"]);

// Check if the connection was successful
if (!$db) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
// Set utf-8 character set
mysqli_set_charset($db, "utf8");
