<?php
// Display menu item as activated if it's a current page
function printMenuItem(string $current, string $path, string $iconTitle): void
{
    // Check if current by looking at the file name. Reference: https://stackoverflow.com/a/7852489/5856760
    $isCurrent = $current . '.php' == basename($_SERVER['REQUEST_URI']);

    // i
    $i_class = 'class="material-icons ' . ($isCurrent ? 'mdl-button--accent"' : '"');
    $i_style = 'style="margin-right: 0.5em;' . ($isCurrent ? '"' : 'color: rgb(93,93,93);"');
    $i = '<i ' . $i_class . ' ' . $i_style . '>' . $iconTitle . '</i>';
    // a tag
    $a_href = 'href="' . $path . '"';
    $a_class = 'class="mdl-navigation__link' . ($isCurrent ? ' mdl-navigation__link--current"' : '"');
    $a = '<a ' . $a_href . ' ' . $a_class . ' style="display: flex; align-items: center">' . $i . ucfirst($current) . '</a>';

    echo ($a);
}
?>

<div class="mdl-layout__drawer drawer-solid main-bg">
    <?php
    $i_class = 'class="material-icons mdl-button--accent v-align"';
    $i_style = 'style="margin-right: 0.5em;"';
    $i = '<i ' . $i_class . ' ' . $i_style . '> local_library </i>'; ?>

    <a class="mdl-layout-title mdl-navigation__link v-align" href="index.php"><?php echo $i; ?>Book Library</a>
    <nav class="mdl-navigation">
        <?php
        printMenuItem("library", "library.php", "apps");
        printMenuItem("add", "add.php", "library_add");
        ?>
    </nav>
</div>