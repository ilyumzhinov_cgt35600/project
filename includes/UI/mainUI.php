<?php
?>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer">
    <?php
    include_once("includes/PHP-functions/utilityFunctions.php");
    include(ROOT . "includes/UI/menuUI.php");
    include_once(ROOT . "includes/PHP-functions/pageTitle.php");

    /** Displays page title with subtitle */
    $displayPageTitleOpt = drawnTitle($pageTitle);
    ?>

    <!-- Reference: https://getmdl.io/components/index.html#layout-section -->
    <main class="mdl-layout__content">
        <div class="mdl-grid">