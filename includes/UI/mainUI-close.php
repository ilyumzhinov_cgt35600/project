            <!-- Display error as Toast on all pages. Reference: https://getmdl.io/components/index.html#snackbar-section -->
            <div class="mdl-js-snackbar mdl-snackbar">
                <div class="mdl-snackbar__text"></div>
                <button type="button" class="mdl-snackbar__action"></button>
            </div>
        </div>
    </main>
</div>