<meta charset="utf-8">
<title><?php echo ($pageTitle); ?></title>

<!-- Reference: https://getmdl.io/started/index.html#use-components -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-blue.min.css">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<!-- Reference: https://www.tutorialsplane.com/material-design-lite-table-full-width/ -->
<style>
    a {
        outline: none;
        text-decoration: none;
    }

    .full-width {
        width: 100%;
    }

    .drawer-solid {
        box-shadow: none;
    }

    .main-bg {
        background: white;
    }

    /* Class for iOS style buttons */
    .round-button {
        border-radius: 300px;
        box-shadow: none;
    }

    /* Class for outlined blocks */
    .layout-stroke {
        border-radius: 5px;
        padding: 20px;
        border: 1px solid rgb(222, 222, 222);
    }

    /* Collapsed content */
    .collapsible {
        text-align: left;
        border: none;
        outline: none;
        font-size: 15px;
    }

    .secondary {
        color: lightgrey;
    }

    /* Aligns list elements vertically */
    .v-align {
        display: flex;
        align-items: center
    }
</style>