<?php

// SERVES FILE DOWNLOADS GIVEN URL AND FILEPATH

include_once("../includes/PHP-functions/utilityFunctions.php");

$bookPath = $_GET["file"];

if (!isset($bookPath)) {
    http_response_code(400);
    return;
}
$bookPath = ROOT . "books/" . $bookPath;

// Send a file in HTTP header
// Reference: https://stackoverflow.com/a/48628477/5856760,
// https://stackoverflow.com/questions/38610331/error-occured-when-opening-epub-from-php-header-epub-file-using-epub-js-library
header('Content-Description: File Transfer');
header('Content-Type: application/epub+zip');
header('Content-Disposition: attachment; filename=' . basename($bookPath));

ob_clean();
flush();
readfile($bookPath);
