<?php
include_once("includes/PHP-functions/utilityFunctions.php");
// Import code to display edit forms GUI
include(ROOT . "includes/PHP-functions/bookForm.php");
include(ROOT . "includes/PHP-functions/editionForm.php");
include(ROOT . "includes/PHP-functions/translationForm.php");

$pageTitle = "Add Book";

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include(ROOT . "includes/UI/headHTML.php"); ?>

    <!-- JQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    <!-- Collapsible elements -->
    <style>
        .content,
        .tabActions {
            display: none;
        }
    </style>
</head>

<body>
    <?php
    include(ROOT . "includes/UI/mainUI.php");
    echo $displayPageTitleOpt(null);
    ?>

    <?php
    // MARK: Button actions
    // Reference: https://stackoverflow.com/a/942826
    $btnNext = displaySubmitBtn("Next", null);
    $btnPrevious = function ($id) {
        return displayActionButton("Previous", 'showNext(' . $id . ')', null);
    };
    $btnSubmit = displayActionButton("Add", 'submitForms()', null);
    ?>

    <!-- MARK: Display forms -->
    <div class="layout-stroke mdl-cell mdl-cell--12-col">
        <?php
        echo displayBookForm(null, 0, true, [$btnNext], ["onsubmit" => 'return showNext(0)'])
        ?>
    </div>
    <div class="layout-stroke mdl-cell mdl-cell--12-col">
        <?php
        echo displayEditionForm(null, null, 1, true, [$btnPrevious(0), $btnNext], ["onsubmit" => 'return showNext(1)'])
        ?>
    </div>
    <div class="layout-stroke mdl-cell mdl-cell--12-col">
        <?php
        echo displayTranslationForm(null, null, 2, true, [$btnPrevious(1), $btnSubmit], null, null)
        ?>
    </div>

    <?php include(ROOT . "includes/UI/mainUI-close.php"); ?>
</body>

<script>
    <?php include(ROOT . "includes/JS/toast.php"); ?>

    // MARK: Multiple step tabs
    showTab(0);

    // Displays a tab based on the content id.
    // Reference: https://www.w3schools.com/howto/howto_js_form_steps.asp
    // Reference: https://www.w3schools.com/howto/howto_js_collapsible.asp
    function showTab(n) {
        var content = document.getElementsByClassName("content")[n];
        var actions = document.getElementById("tabActions-" + n);

        content.style.display = (content.style.display || "none") == "none" ? "inline" : "none";
        actions.style.display = (actions.style.display || "none") == "none" ? "inline" : "none";
    }

    // Reveals next tab while closing the current one. Input is current tab index which get incremented by 1.
    function showNext(n) {
        var tabs = document.getElementsByClassName("collapsible");

        showTab(n);

        if (n < tabs.length) {
            showTab(n + 1);
        }

        return false;
    }

    // MARK Submit multiple forms
    // Submits multiple forms.
    // Reference: https://stackoverflow.com/a/48759069/5856760
    function submitForms() {
        // Get all input data pairs from ALL forms.
        var datastring = $("#editForm-0").serializeArray();
        datastring = datastring.concat($("#editForm-1").serializeArray());
        datastring = datastring.concat($("#editForm-2").serializeArray());

        // Redirect with POST. Reference: https://stackoverflow.com/a/28532801/5856760
        function redirectPost(location, args) {
            var form = '';
            $.each(args, function(key, value) {

                form += '<input type="hidden" name="' + value.name + '" value="' + value.value + '">';
                form += '<input type="hidden" name="' + key + '" value="' + value.value + '">';
            });
            $('<form action="' + location + '" method="POST">' + form + '</form>').submit();
        }

        redirectPost('processing/doAdd.php', datastring);
    }
</script>

</html>