## Set up config.json file

1. Make a copy of the "config-template.json" file and name it "config.json".
2. Open the "config.json" file and replace values accordingly:
   1. Replace fields where values are in "<>" brackets.



## Optionally: Generate a new AES-256 private key in PHP

Reference: https://www.php.net/manual/en/function.bin2hex.php



1. Generate 32 random bytes (256 bits) and encode them into base64:

```php
$key = base64_encode(openssl_random_pseudo_bytes(32));
```

