<?php

// PROCESSES BOOK->EDITION->TRANSLATION CREATION

include_once("../includes/PHP-functions/utilityFunctions.php");

// MARK: Validate request
$elementsKeys = [
    "book_title" => 1, "book_author" => 1, "book_description" => 0, "book_release_year" => 1,
    "ed_name" => 1, "ed_description" => 0, "ed_release_year" => 1,
    "trans_lang_code" => 1, "trans_translator" => 0, "trans_title" => 0, "trans_epub" => 0, "trans_web" => 0
];
try {
    // Array of all keys and retrieved values
    $posted = validateRetrievedValues($_POST, $elementsKeys, 'processInput');
} catch (Exception $e) {
    returnError($e->getMessage(), 'add.php');
}

include(ROOT . "includes/openDBconn.php");
// MARK: Query 1: Insert book
$queryInsertBook = "insert into BOOK(title, author, description, release_year) values ('" . $posted["book_title"] . "', '" . $posted["book_author"] . "', '" . $posted["book_description"] . "', '" . $posted["book_release_year"] . "');";
if (!mysqli_query($db, $queryInsertBook)) {
    returnError("Failed to insert the book", "add.php");
}
// Retrieve generated ID. Reference: https://www.php.net/manual/en/mysqli.insert-id.php
$book_id = mysqli_insert_id($db);
//
// MARK: Query 2: Insert edition
$queryInsertEdition = "insert into BOOK_EDITION(book_id, edition_id, ed_name, description, release_year) values (" . $book_id . ", null, '" . $posted["ed_name"] . "', '" . $posted["ed_description"] . "', '" . $posted["ed_release_year"] . "');";
if (!mysqli_query($db, $queryInsertEdition)) {
    returnError("Failed to insert the book edition", "add.php");
}
$edition_id = mysqli_insert_id($db);
//
// MARK: Query 3: Insert translation
$queryInsertTranslation = "insert into BOOK_TRANSLATION(book_id, edition_id, lang_code, translator, title, epub_path, web_path) values ('" . $book_id . "', '" . $edition_id . "', '" . $posted["trans_lang_code"] . "', '" . $posted["trans_translator"] . "', '" . $posted["trans_title"] . "', '" . $posted["trans_epub"] . "', '" . $posted["trans_web"] . "');";
if (!mysqli_query($db, $queryInsertTranslation)) {
    returnError("Failed to insert the book translation", "add.php");
}
$lang_code = $posted["trans_lang_code"];
//
include(ROOT . "includes/closeDBconn.php");

// MARK: Redirect
header("Location:" . "/view.php?book=" . $book_id . "&edition=" . $edition_id . "&lang=" . $lang_code);
