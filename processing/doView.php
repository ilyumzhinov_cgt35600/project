<?php

// OPENS A DEFAULT EDITION AND TRANSLATION OF BOOK

// MARK: Includes
include_once("../includes/PHP-functions/utilityFunctions.php");

// Retrieve book
$bookID = isset($_GET["book"]) ? $_GET["book"] : null;
// Retrieve edition or default
$editionID = $_GET["edition"] ?? 1;

// Check for errors
if (!isset($bookID)) {
    setcookie("errorMessage", "You need to specify a book", time() + 2);
    header("Location:library.php");
    exit;
}

if (!isset($_GET["lang"])) {
    include(ROOT . "includes/openDBconn.php");
    // MARK: Query 1: Select from BOOK_TRANSLATION
    $query = "select * from BOOK_TRANSLATION where book_id=" . $bookID;
    $queryResults = mysqli_query($db, $query);

    $queryOutput = [];
    while ($translationRow = mysqli_fetch_array($queryResults)) {
        $queryOutput = array_merge($queryOutput, [$translationRow]);
    }
    $bookTranslations = $queryOutput;
    //
    include(ROOT . "includes/closeDBconn.php");

    // FILTER editions -> SORT BY lang_code -> SELECT lang_code -> SELECT first
    $editions_filtered = selectRow($bookTranslations, ["edition_id" => $editionID]);
    $editions_sorted = sorting($editions_filtered, comparedBy("lang_code"));

    $defaultTranslation = selectColumn("lang_code", $editions_sorted, null)[0];
}

// Retrieve translation
$lang = isset($_GET["lang"]) ? $_GET["lang"] : $defaultTranslation;

header("Location:/view.php?book=" . $bookID . "&edition=" . $editionID . "&lang=" . $lang);
