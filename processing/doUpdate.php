<?php

// PROCESSES BOOK|EDITION|TRANSLATION UPDATES

include_once("../includes/PHP-functions/utilityFunctions.php");

$updateMode = $_GET["mode"];

// MARK: Update mode
switch ($updateMode) {
    case "book":
        // MARK: Validate: book request
        $elementsKeys = [
            "book_id" => 1, "title" => 0, "author" => 0, "description" => 0, "release_year" => 1
        ];
        try {
            // Array of all keys and retrieved values
            $posted = validateRetrievedValues($_POST, $elementsKeys, 'processInput');
        } catch (Exception $e) {
            returnError('Failed to update a lang', 'library.php');
        }

        updateBook($posted["book_id"], $posted["title"], $posted["author"], $posted["description"], $posted["release_year"]);
        break;
    case "edition":
        // MARK: Validate: edition request
        $elementsKeys = [
            "book_id" => 1,
            "edition_id" => 1, "ed_name" => 1, "ed_description" => 0, "ed_release_year" => 0
        ];
        try {
            // Array of all keys and retrieved values
            $posted = validateRetrievedValues($_POST, $elementsKeys, 'processInput');
        } catch (Exception $e) {
            returnError('Failed to update edition', 'library.php');
        }

        updateEdition($posted["book_id"], $posted["edition_id"], $posted["ed_name"], $posted["ed_description"], $posted["ed_release_year"]);
        break;
    case "lang":
        // MARK: Validate: lang request
        $elementsKeys = [
            "book_id" => 1,
            "edition_id" => 1,
            "trans_lang_code" => 1, "trans_translator" => 0, "trans_title" => 0, "trans_epub" => 0, "trans_web" => 0
        ];
        try {
            // Array of all keys and retrieved values
            $posted = validateRetrievedValues($_POST, $elementsKeys, 'processInput');
        } catch (Exception $e) {
            returnError('Failed to update lang', 'library.php');
        }

        updateLang($posted["book_id"], $posted["edition_id"], $posted["trans_lang_code"], $posted["trans_translator"], $posted["trans_title"], $posted["trans_epub"], $posted["trans_web"]);

        break;
    default:
        returnError("Did not receive required data values", "library.php");
}

function updateBook($book_id, $title, $author, $description, $release_year)
{
    // MARK: Query: Update BOOK
    include(ROOT . "includes/openDBconn.php");
    $queryUpdate = "update BOOK set book_id=" .  $book_id . ", title='" . $title . "', author='" . $author . "', description='" . $description . "', release_year=" . $release_year . " where book_id=" . $book_id;
    $queryResult = mysqli_query($db, $queryUpdate);
    //
    include(ROOT . "includes/closeDBconn.php");

    if (!$queryResult) {
        returnError("Failed to update the book", "update.php?book=" . $book_id);
    }

    returnError("Book updated!", "processing/doView.php?book=" . $book_id);
}

function updateEdition($book_id, $edition_id, $ed_name, $description, $release_year)
{
    // MARK: Query: Update BOOK_EDITION
    include(ROOT . "includes/openDBconn.php");
    $queryUpdate = "update BOOK_EDITION set book_id=" .  $book_id . ", edition_id=" . $edition_id . ", ed_name='" . $ed_name . "', description='" . processInput($description) . "', release_year=" . $release_year . " where book_id=" . $book_id . " and edition_id=" . $edition_id;
    $queryResult = mysqli_query($db, $queryUpdate);
    //
    include(ROOT . "includes/closeDBconn.php");

    if (!$queryResult) {
        returnError("Failed to update the edition", "update.php?book=" . $book_id . '&edition=' . $edition_id);
    }

    returnError("Edition updated!", "processing/doView.php?book=" . $book_id . '&edition=' . $edition_id);
}

function updateLang($book_id, $edition_id, $lang_code, $translator, $title, $epub_path, $web_path)
{
    // MARK: Query: Update BOOK_TRANSLATION
    include(ROOT . "includes/openDBconn.php");
    $queryUpdate = "update BOOK_TRANSLATION set book_id=" .  $book_id . ", edition_id=" . $edition_id . ", lang_code='" . $lang_code . "', translator='" . $translator . "', title='" . $title . "', epub_path='" . $epub_path . "', web_path='" . $web_path . "' where book_id=" . $book_id . " and edition_id=" . $edition_id . " and lang_code='" . $lang_code . "'";
    $queryResult = mysqli_query($db, $queryUpdate);
    //
    include(ROOT . "includes/closeDBconn.php");

    if (!$queryResult) {
        returnError("Failed to update the language", "update.php?book=" . $book_id . '&edition=' . $edition_id . '&lang=' . $lang_code);
    }
    
    returnError("Language updated!", "processing/doView.php?book=" . $book_id . '&edition=' . $edition_id . '&lang=' . $lang_code);
}
