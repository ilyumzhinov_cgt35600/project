<?php

// PROCESSES CREATE LOGIN REQUESTS

include_once("../includes/PHP-functions/utilityFunctions.php");

// MARK: Validate request
$elementsKeys = [
    "userID" => 1, "password" => 1, "passwordConf" => 1, "name" => 1, "email" => 1
];
try {
    // Array of all keys and retrieved values
    $posted = validateRetrievedValues($_POST, $elementsKeys, 'processInput');
} catch (Exception $e) {
    returnError("Failed to create user", "loginCreate.php");
}

// MARK: Declare values
$userID = $posted["userID"];
$psw1 = $posted["password"];
$psw2 = $posted["passwordConf"];
$name = $posted["name"];
$email = $posted["email"];

// MARK: Encrypt values
$salt = get_salt();
$email_enc = encrypt_standard($email, $salt);

// MARK: Hash values
$psw1_hash = hash_standard($psw1, $salt);
$psw2_hash = hash_standard($psw2, $salt);

// Check passwords match
if ($psw1_hash != $psw2_hash) {
    returnError("Passwords do not match!", "loginCreate.php");
}

include(ROOT . "includes/openDBconn.php");
// MARK: Query 1: Insert user
$queryInsertBook = "insert into USER(userID, password, name, email, salt) values ('" . $userID . "', '" . $psw1_hash . "', '" . $name . "', '" . $email_enc . "', '" . $salt . "');";
$queryResut = mysqli_query($db, $queryInsertBook);
//
include(ROOT . "includes/closeDBconn.php");

if (!$queryResut) {
    returnError("Failed to insert the user", "loginCreate.php");
}

returnError("User added: " . $userID, "index.php");
