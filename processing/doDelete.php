<?php

// PROCESSES BOOK(+)EDITION(+)TRANSLATION REQUESTS

include_once("../includes/PHP-functions/utilityFunctions.php");

// MARK: Validate request
$elementsKeys = ["book_id" => 1];
try {
    // Array of all keys and retrieved values
    validateRetrievedValues($_POST, $elementsKeys, null);
} catch (Exception $e) {
    returnError("Failed to delete", "library.php");
}

// MARK: Declare values
$book_id = $_POST["book_id"];
$edition = isset($_POST["edition_id"]) ? " and edition_id=" . $_POST["edition_id"] . "" : "";
$lang_code = isset($_POST["lang_code"]) ? " and lang_code='" . $_POST["lang_code"] . "'" : "";

include(ROOT . "includes/openDBconn.php");
// MARK: Prepare Query BOOK_TRANSLATION: Delete
$queryDeletion = "delete from BOOK_TRANSLATION where (book_id=" . $book_id . $edition . $lang_code . ")";
if (!mysqli_query($db, $queryDeletion)) {
    returnError("Failed to delete Book translations", "processing/doView.php?book=" . $book_id . "&edition=" . $_POST["edition_id"] . "&lang=" . $_POST["lang_code"]);
}
//
// MARK: Prepare Query BOOK_EDITION: Delete
if ($_POST["lang_code"] == null) {
    $queryDeletion = "delete from BOOK_EDITION where (book_id=" . $book_id . $edition . ")";
    if (!mysqli_query($db, $queryDeletion)) {
        returnError("Failed to delete Book editions", "processing/doView.php?book=" . $book_id . "&edition=" . $_POST["edition_id"]);
    }
}
//
// MARK: Prepare Query BOOK: Delete
if ($_POST["lang_code"] == null and $_POST["edition_id"] == null) {
    $queryDeletion = "delete from BOOK where (book_id=" . $book_id . ")";
    if (!mysqli_query($db, $queryDeletion)) {
        returnError("Failed to delete the Book", "processing/doView.php?book=" . $book_id);
    }
}
//
include(ROOT . "includes/closeDBconn.php");

returnError("Successfully deleted", "library.php");
